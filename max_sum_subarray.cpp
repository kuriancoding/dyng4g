#include "dynamic.h"

int maxSum(int array[], int length)
{
    int max_sum = 0, sum_till_now = 0;
    for (int i = 0; i < length; i++) {
	sum_till_now = sum_till_now + array[i];
	if (sum_till_now < max_sum) {
	    sum_till_now = max_sum;
	} else {
	    max_sum = sum_till_now;
	}
    }
    return max_sum;
}

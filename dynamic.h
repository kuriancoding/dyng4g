#include <string.h>
#include <iostream>
#define M 3
#define N 3

using namespace std;
int editDistance(string a, string b, int c, int d);
int lis(int array[], int begin, int length);
int maxSum(int array[], int length);
int minCost(int array[M][N], int m, int n);
int count(int arr[], int m, int n);
int min3(int a, int b, int c);
int bino(int n, int m);
int longestCommonSubsequence(string a, int lenA, string b, int lenB);
int fib(int n);
void MinSub(int array[5][5], int size);

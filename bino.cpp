#include "dynamic.h"
int bino(int n, int m) {
	if (m > n) {
		return 0;
	}
	if (m == 0 || n == m) {
		return 1;
	}
	return bino(n - 1, m - 1) + bino(n - 1, m);
}

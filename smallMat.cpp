#include "dynamic.h"

int minimum(int a, int b, int c) { return min(min(a, b), min(b, c)); }

void MinSub(int array[5][5], int size) {
	int candidate[size][size];
	for (int k = 0; k < size; k++)
		for (int h = 0; h < size; h++) candidate[h][k] = 0;
	for (int q = 0; q < size; q++) candidate[q][0] = array[q][0];
	for (int q = 0; q < size; q++) candidate[0][q] = array[0][q];
	for (int i = 1; i < size; i++)
		for (int j = 1; j < size; j++) {
			if (array[i][j] == 1) {
				candidate[j][i] =
				    minimum(candidate[i - 1][j],
					    candidate[i][j - 1],
					    candidate[i - 1][j - 1]) +
				    1;
			}
		}
	int Max = 0, maxRow = 0, maxCol = 0;
	for (int l = 0; l < size; l++)
		for (int m = 0; m < size; m++) {
			if (Max < candidate[l][m]) {
				Max = candidate[l][m];
				maxRow = l;
				maxCol = m;
			}
		}
	cout << Max << " " << maxRow << "," << maxCol;
	return;
}

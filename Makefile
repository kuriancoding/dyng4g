CC=g++
# Wfatal-errors: for compilation to halt if there is even one error
# Werror: to count even warnings as errors
# Wall: for notices and compilation messages along with error and warnings
FLAGS=-Wall -Wfatal-errors -Werror
.cpp:
	$(CC) $< $(FLAGS) -o bin/$@
make:
	$(CC) *.cpp $(FLAGS) -o bin/test

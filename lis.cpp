#include "dynamic.h"
int lis(int array[], int begin, int length)
{
    if (length - begin == 1)
	return 1;
    int len = 0, temp = 0, maxInt;
    for (int i = begin + 1; i < length; i++) {
	if (array[i] < array[i + 1]) {
	    temp++;
	} else {
	    temp = 0;
	    maxInt = i;
	    break;
	}
	if (len < temp)
	    len = temp;
    }
    if (temp == 0)
	return max(lis(array, maxInt + 1, length), lis(array, begin, maxInt));
    return len;
}

#include "dynamic.h"
int longestCommonSubsequence(string a, int lenA, string b, int lenB) {
	/*compare last characters of both strings, if they match then increment
	the subsequence length count by 1 and pass it on the the next function,
	if the count does not match, pass it on as the max of two substrings*/
	if (lenA == 0 || lenB == 0) {
		return 0;
	}
	if (a[lenA - 1] == b[lenB - 1]) {
		return 1 + longestCommonSubsequence(a, lenA - 1, b, lenB - 1);
	}
	return max(longestCommonSubsequence(a, lenA - 1, b, lenB),
		   longestCommonSubsequence(a, lenA, b, lenB - 1));
}

#include "dynamic.h"

int min3(int a, int b, int c) { return min(min(a, b), min(b, c)); }

int minCost(int array[M][N], int m, int n)
{
    if (n < 0 || m < 0)
	return 1;

    if (m == 0 && n == 0)
	return array[m][n];

    return array[m][n] + min3(minCost(array, m - 1, n),
			     minCost(array, m, n - 1),
			     minCost(array, m - 1, n - 1));
}
